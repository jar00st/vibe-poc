<?php

namespace App\Http\Controllers;

use App\Http\Services\TequilaService;
use Illuminate\Http\Request;

class TequilaController extends Controller
{
    protected TequilaService $tequilaService;

    public function __construct(TequilaService $tequilaService)
    {
        $this->tequilaService = $tequilaService;
    }
    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required|string|min:1',
        ]);


        $results = $this->tequilaService->searchLocations($request->search);

        return response()->json($results);
    }
}
