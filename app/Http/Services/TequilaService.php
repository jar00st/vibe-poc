<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Http;

class TequilaService
{
    protected $baseUrl = 'https://api.tequila.kiwi.com';
    protected $apiKey;

    public function __construct()
    {
        $this->apiKey = env('TEQUILA_API_KEY');
    }

    public function searchLocations($term)
    {
        $response = Http::withHeaders([
            'accept' => 'application/json',
            'apikey' => $this->apiKey,
        ])->get("{$this->baseUrl}/locations/query", [
            'term' => $term,
            'locale' => 'en-US',
            'location_types' => 'airport',
            'limit' => 10,
            'active_only' => true,
        ]);

        return $response->json();
    }
}
