# Nombre del Proyecto

Descripción breve del proyecto.

## Requisitos Previos

Asegúrate de tener instalado:

- [PHP](https://www.php.net/) (versión recomendada 7.4 o superior)
- [Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/) (incluyendo NPM)

## Instalación

Sigue estos pasos para configurar el proyecto en tu entorno local.

### Clonar el Repositorio

Primero, clona el repositorio a tu máquina local:

```bash
git clone https://gitlab.com/jar00st/vibe-poc
```
Instalar Dependencias de Laravel

Navega al directorio del proyecto y ejecuta Composer para instalar las dependencias de PHP:

```bash
cd vibe-poc
composer install
```
Configuración del Entorno

Copia el archivo .env.example a .env y ajusta las configuraciones según tu entorno:

```bash
cp .env.example .env
```
Genera una clave para la aplicación:

```bash
php artisan key:generate
```
Instalar Dependencias de NPM

Instala las dependencias de Node.js necesarias para el frontend:

```bash
npm install
```
Instalar Laravel Breeze con Inertia

Para instalar Laravel Breeze con el stack Inertia.js, ejecuta:

```bash
php artisan breeze:install vue

npm install
```
##Compilación de Assets

Para compilar los assets y habilitar el hot-reloading, ejecuta:

```bash
npm run dev
```
Ejecutar el Servidor de Desarrollo

Inicia el servidor de desarrollo de Laravel:

```bash
php artisan serve
```
Acceder a la Aplicación

Abre tu navegador y ve a http://127.0.0.1:8000 para ver la aplicación.
